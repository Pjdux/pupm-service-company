import styled from "styled-components";

const FormWrap = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  margin: 0 auto;

  color: #222;
  label {
    color: #fff;
  }
  input {
    padding: 5px;
    border-radius: 4px;
    color: #222 !important;
    width: 200px;
  }

  .select,
  select {
    color: #222 !important;
    width: 200px;
  }
  .phone {
    color: #222;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin: 5px auto;
  }
`;

export default FormWrap;
