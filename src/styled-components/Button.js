import styled from "styled-components";

const Button = styled.div`
  margin: 20px auto;
  border: 2px solid #09487b;
  border-radius: 7px;
  font-size: 0.8em;
  padding: 20px;
  font-family: "Roboto", sans-serif;
  text-decoration: none;
  color: #09487b;
  width: 96%;
  max-width: 200px;

  &:hover {
    transition: linear 0.1s;
    background-color: #09487b;
    color: white;
  }
`;

export default Button;
