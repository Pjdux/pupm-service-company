import styled from "styled-components";

const MainLayout = styled.footer`
  font-family: "Roboto", sans-serif;
  width: 95%;
  margin: 100px auto;
  max-width: 1000px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default MainLayout;
