import React, { useState, useEffect } from "react";
import StoryWrapper from "../styled-components/StoryWrapper";
import CarForm from "./forms/CarForm";
import "fontsource-roboto";
import Box from "@material-ui/core/Box";
import Fly from "../assets/fly-img.png";
import StoryButton from "./ui-components/buttons/StoryButton";
import GhButton from "./ui-components/buttons/GHButton";

let today = new Date();

let date =
  today.getMonth() + 1 + "-" + today.getDate() + "-" + today.getFullYear();

const Story = ({ posts }) => {
  const [ghData, setGhData] = useState({});
  const [moreData, setMoreData] = useState([]);

  useEffect(() => {
    const url = "https://api.github.com/users/pjdaze";
    const displayData = async () => {
      const res = await fetch(url);
      const data = await res.json();

      setGhData({ ...data });
    };

    displayData();
  }, []);

  const getGhData = () => {
    setMoreData(
      <div>
        <img src={ghData.avatar_url} alt="" />
        <p>Name: {ghData.name} </p>
        I've got{" "}
        <span className="repo-num">
          <a href="https://github.com/Pjdaze?tab=repositories">
            {ghData.public_repos}
          </a>
        </span>{" "}
        {""}
        repos.
        <span>
          <p>You can check em Out Here</p>
          <a href={"https://github.com/Pjdaze?tab=repositories"}>
            Repos Page
          </a>{" "}
          / <a href={ghData.repos_url}>Raw</a>
        </span>
      </div>
    );
    console.log("clicked", ghData);
  };

  return (
    <StoryWrapper>
      <h1>
        Interest Letter <br /> <span id="my-name">Pedro Diaz</span>
      </h1>
      <h2>Pump Service Company</h2>

      <span className="divider" />

      <div className="intro-box">
        <div className="blah">
          <p>
            Hello Everyone! Pedro Diaz Here, First of all, I want to thank you
            for taking the time to review this app, I Believe with my experience
            and the current situation, I would be a great fit for the company. I
            go to school At Valencia part-time to get certified as a programmer
            specializing in C++. I decided to do this because I’ve always wanted
            to get my feet wet with a lower-level language and I felt ready!.
            <br />
            That being said, I’m a web/mobile developer at heart, I Have 4 years
            of profecional experience, and i love to accomplish my goals using
            Javascript. There is nothing better for me that a beautiful and
            intuitive application or website so needless to say I love great
            design in all aspects.
            <br /> <br />
            I'm absolutely passionate about Javascript and CSS and have been
            focusing on React.js as my front-end library of choice, and Node.js
            for backend solutions. <br /> <br />
            I'm Proficient in a CSS pre-processors including SASS, LESS. I'm
            very comfortable with MongoDB as well as FaunaDB and familiar MySQL.
            <br />
            <br />
            When Using React.js , I use Styled-Components as my go-to because of
            all the great features, also if needed, I have no problem using
            Semantic-UI, (my favorite), Material-UI or Bootstrap ect.
            <br />
          </p>
          <hr style={{ opacity: "0.5" }} />
        </div>
      </div>
      <span className="divider" variant="middle" />
      <div className="intro-box">
        <Box style={{ margin: "100px  auto" }}>
          <h2>My GitHub</h2>
          <h3>Data Fetching</h3>
          <p>
            My GitHub has all my public repos Click below to fetch Github data.{" "}
            <br />
            <br />
          </p>

          <div className="github-data">
            <div>{moreData}</div>

            <GhButton setOnClick={getGhData} />
          </div>
        </Box>
      </div>
      <span className="divider" variant="middle" />

      <div className="intro-box">
        <Box style={{ margin: "100px  auto" }}>
          <h2>React-Select</h2>

          <h3 id="assesment">Previous Employer Take-home Test</h3>

          <p>
            This Was my most recent take-home coding test I had at a coding
            interview, to displaying select data depending on the previous
            select value and printing all the values to the console using
            React.js
            <br />
            So for the form, I'm using react select for easy access to values
            and I'm displaying the make and model differs depending on the
            currently selected option value.
            <br />
            <br />
            <h3>
              This links showed me how easy it is to use this library, and all
              of it's great features
            </h3>
            <div className="button-box flex-center">
              <StoryButton>
                <a href="https://react-select.com/home">React-Select</a>
              </StoryButton>
              <StoryButton>
                <div className="button-box flex-center">
                  <a href="https://react-select.com/home/async">
                    React-Select/async
                  </a>
                </div>
              </StoryButton>
            </div>
          </p>

          <CarForm />

          <div className="button-box flex-center">
            {" "}
            Check the repo for this entire project <br />
            <a
              href="https://gitlab.com/Pjdux/pupm-service-company"
              target="_blank"
            >
              Here
            </a>
          </div>
          <h2>Contact Me</h2>

          <p>
            <a href="https://pjdux.com">My Site</a> or at{" "}
            <a
              href="mailto:pjdazeux@gmail.com?Subject=Hello%20there"
              target="_top"
            >
              pjdazeux@gmail.com
            </a>
          </p>
        </Box>
      </div>
      <div className="footer">
        <p>
          Created On The{" "}
          <a href="https://pjdux.com/projects">
            <img src={Fly} alt="fly-joke" />
          </a>
        </p>
      </div>
    </StoryWrapper>
  );
};

export default Story;
